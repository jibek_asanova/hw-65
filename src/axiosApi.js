import axios from 'axios';

const axiosApi = axios.create({
    baseURL: 'https://jibek-asanova-default-rtdb.firebaseio.com/' // Your URL here!
});

export default axiosApi;